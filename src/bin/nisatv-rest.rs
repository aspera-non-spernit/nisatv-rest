#![feature(proc_macro_hygiene, decl_macro)]
#[macro_use]
extern crate clap;
extern crate handlebars;
extern crate nisatv_models;
extern crate nisatv_rest;
extern crate reqwest;
#[macro_use]
extern crate rocket;
extern crate rocket_cors;
extern crate serde_json;

use chrono::prelude::*;
use clap::App;
use handlebars::Handlebars;
use nisatv_models::{Broadcast, Club, Info, Player, Match, Season, NISA};
use nisatv_rest::{Credentials, DataSource, NisaApp, CRUD};
use rocket::{Data, http::Method, response::NamedFile, State};
use rocket_contrib::{json::Json, serve::StaticFiles, templates::Template};
use rocket_cors::{AllowedOrigins, AllowedHeaders};
use std::{
    collections::HashMap,
    fs::File,
    path::{Path, PathBuf},
};

/// Returns all stored information
#[get("/all")]
fn all(app: State<NisaApp>) -> Json<NISA> {
    Json(app.data().unwrap())
}

/// Returns a complete list of all stored Clubs
#[get("/all/broadcasts")]
fn all_broadcasts(app: State<NisaApp>) -> Json<Vec<Vec<Broadcast>>> {
    let mut bc: Vec<Vec<Broadcast>> = vec![];
    for s in app.data().unwrap().seasons {
        for m in s.matches {
            if m.broadcasts.is_some() {
                bc.push(m.broadcasts.unwrap());
            }
        }
    }
    Json(bc)
}

/// Returns a complete list of all stored Clubs
#[get("/all/clubs")]
fn get_all_clubs(app: State<NisaApp>) -> Json<Vec<Club>> {
    Json(app.data().unwrap().clubs)
}

/// Saves the context
#[post("/all/clubs", format = "json", data = "<data>")]
fn post_all_clubs(app: State<NisaApp>, data: Json<Vec<Club>>) -> Json<String> {
    if let Ok(mut nisa) = app.data() {
        nisa.clubs = data.into_inner();
        match app.data_source.update(nisa) {
            Ok(_) => { Json(String::from("Saving clubs was successful.")) },
            Err(e) => { Json(format!("{:?}", e.kind())) }
        }
    } else {
        Json(String::from("An error occured. Could not access DataSource."))
    }
}

/// Returns a complete list of all stored Matches
#[get("/all/matches")]
fn get_all_matches(app: State<NisaApp>) -> Json<Vec<Match>> {
    let mut matches = vec![];
    for season in app.data().unwrap().seasons {
        for m in season.matches {
            matches.push(m);
        }
    }
    Json(matches)
}

#[get("/all/players")]
fn get_all_players() -> Json<Vec<Player>> {
    let resp = reqwest::get("http://radiant-citadel-22556.herokuapp.com/api/players").unwrap().text().unwrap();
    let players: Vec<Player> = serde_json::from_str(&resp).unwrap();
    Json(players)
}

/// Returns info about all sesons available.
#[get("/all/seasons")]
fn all_seasons(app: State<NisaApp>) -> Json<Vec<Info>> {
    let nisa: NISA = app.data().unwrap();
    let mut infos = vec![];
    for season in nisa.seasons {
        infos.push(season.info);
    }
    Json(infos)
}

/// Returns a ```Club``` given a short code.
#[get("/club?<by_short>")]
fn club_by_short(app: State<NisaApp>, by_short: String) -> Json<Vec<Club>> {
    let nisa: NISA = app.data().unwrap();
    let mut clubs: Vec<Club> = vec![];
    for club in nisa.clubs {
        if club.short == by_short {
            clubs.push(club);
        }
    }
    Json(clubs)
}

// production
// #[post("/admin/dashboard", data="<credentials>", format="application/x-www-form-urlencoded")]
// fn dashboard(credentials: Form<Credentials>) -> Template {
//      let mut context: HashMap<&str, &str> = HashMap::new();
//      if &credentials.password == "k(4lkflkh3öHKkd--." {
//          context.insert("status", "authenticated");
//          context.insert("username", &credentials.username);
//          Template::render("dashboard", context)
//      } else {
//          context.insert("status", "authentication failed");
//          Template::render("login", context)
//      }
// }

/// Returns a vector of ```Match``` given a rfc3339 encoded Date.
/// Returns empty Json with an empty Vec if no ```Match``` is found.
#[get("/match?<by_date>", rank = 1)]
fn match_by_date(app: State<NisaApp>, by_date: String) -> Json<Vec<Match>> {
    let query_date = DateTime::parse_from_rfc3339(&by_date).unwrap();
    let nisa: NISA = app.data().unwrap();
    let mut matches: Vec<Match> = vec![];
    for season in nisa.seasons {
        for m in season.matches {
            if m.date.year() == query_date.year()
                && m.date.month() == query_date.month()
                && m.date.day() == query_date.day()
            {
                matches.push(m);
            }
        }
    }
    Json(matches)
}

/// Returns a vector of ```Match``` given a short code.
#[get("/matches?<by_short>", rank = 2)]
fn match_by_short(app: State<NisaApp>, by_short: String) -> Json<Vec<Match>> {
    let nisa: NISA = app.data().unwrap();
    let matches: Vec<Match> = nisa
        .seasons
        .iter()
        .map(|s| s.matches.clone())
        .flatten()
        .filter(|m| (m.home_short == by_short || m.away_short == by_short))
        .collect();
    Json(matches)
}

#[get("/resource/<file..>")]
fn resource(file: PathBuf) -> Option<NamedFile> {
    NamedFile::open(Path::new("static/assets/").join(file)).ok()
}

///
#[get("/seasons?<by_league_short>")]
fn seasons_by_league(app: State<NisaApp>, by_league_short: String) -> Json<Vec<Season>> {
    let nisa: NISA = app.data().unwrap();
    let mut seasons = vec![];
    for season in nisa.seasons {
        match season
            .info
            .name
            .to_lowercase()
            .find(&by_league_short.to_lowercase())
            {
                Some(_) => seasons.push(season),
                None => {}
            }
    }
    Json(seasons)
}

#[post("/upload?<short>", data="<image>")]
fn upload(image: Data, short: String) -> std::io::Result<Json<String>> {
    let filename = format!("/assets/club_logo{short}", short = short);
    match image.stream_to_file(Path::new(&filename)) {
        Ok(_r) => {},
        Err(_e) => {}
    }
    Ok(Json(String::from("An error occured. Could not access DataSource.")))
}
/// PAGES

/// The admin login page
#[get("/admin/login")]
fn admin_login() -> Template {
    let context: HashMap<&str, &str> = HashMap::new();
    Template::render("login", context)
}

#[get("/admin/edit/clubs")]
fn edit_clubs(app: State<NisaApp>) -> Template {
    let mut context: HashMap<&str, (Vec<Club>, u8)> = HashMap::new();
    let nisa: NISA = app.data().unwrap();
    context.insert("context", (nisa.clubs, 1) );
    Template::render("edit_clubs", context)
}

#[get("/admin/edit/matches")]
fn edit_matches(app: State<NisaApp>) -> Template {
    let mut context: HashMap<&str, (Vec<Club>, Vec<Season>)> = HashMap::new();
    let nisa: NISA = app.data().unwrap();
    context.insert("context", (nisa.clubs, nisa.seasons) );
    Template::render("edit_matches", context)
}

#[get("/admin/edit/players")]
fn edit_players() -> Template {
    let mut context: HashMap<&str, Vec<Player> > = HashMap::new();
    let players: Vec<Player> = get_all_players().to_vec();
    context.insert("players", players );
    Template::render("edit_players", context)
}

/// Shows a usage help page
#[get("/help")]
fn help() -> Template {
    let context: HashMap<&str, &str> = HashMap::new();
    Template::render("help", context)
}
#[get("/")]
fn index() -> Template {
    let mut context: HashMap<&str, &str> = HashMap::new();
    context.insert("message", "See /api/help for more info.");
    Template::render("index", context)
}

fn app() -> std::io::Result<NisaApp> {
    let yaml = load_yaml!("../../config/cli.yml");
    let opts = App::from_yaml(yaml).get_matches();
    if let Some(data) = opts.value_of("data") {
        match File::open(&data) {
            Ok(_f) => {
                let ds = DataSource::file(None, data.to_string());
                Ok(
                    NisaApp {
                        data: Some(ds.read()?),
                        data_source: ds
                    }
                )
                // if let Some(_) = opts.value_of("memory") {
                //     Ok (
                //         NisaApp {
                //             data: Some(ds.read()?),
                //             data_source: ds
                //         }
                //     )
                // } else {
                //     Ok(
                //         NisaApp {
                //             data: None,
                //             data_source: ds
                //         } 
                //     )
                // }
            },  
            Err(e) => Err(e)
        }
    } else {
        panic!("Data Source required.")
    }
}

fn main() {
    let nisa_app = app().unwrap();
    let mut handlebars = Handlebars::new();
    handlebars.set_strict_mode(true);

    let cors = rocket_cors::CorsOptions {
        //allowed_origins: AllowedOrigins::some_exact(&["https://nisatv.network", "https://localhost:8000"]),
        allowed_origins: AllowedOrigins::all(),

        allowed_methods: vec![Method::Get, Method::Post].into_iter().map(From::from).collect(),
        allowed_headers: AllowedHeaders::some(&["Authorization", "Accept"]),
        allow_credentials: true,
        ..Default::default()
    }
    .to_cors().unwrap();
    rocket::ignite()
        .manage(nisa_app)
        .mount(
            "/assets",
            StaticFiles::from(concat!(env!("CARGO_MANIFEST_DIR"), "/static/assets")),
        )
        .mount(
            "/scripts",
            StaticFiles::from(concat!(env!("CARGO_MANIFEST_DIR"), "/static/scripts")),
        )
        .mount(
            "/styles",
            StaticFiles::from(concat!(env!("CARGO_MANIFEST_DIR"), "/static/styles")),
        )
        .mount(
            "/",
            routes![
                admin_login,
                all,
                all_broadcasts,
                get_all_clubs,
		post_all_clubs,
                get_all_players,
                get_all_matches,
                all_seasons,
                club_by_short,
                edit_clubs,
		edit_matches,
                edit_players,
                help,
                match_by_date,
                match_by_short,
                index,
                resource,
                seasons_by_league,
                upload
            ],
        )
        .attach(cors)
        .attach(Template::fairing())
        .launch();
}
