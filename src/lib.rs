// extern crate nisatv_data;
extern crate nisatv_models;
extern crate serde_json;

// use nisatv_data::{CRUD, DataSource, DataSourceType };
use nisatv_models::NISA;
use std::{fs::{File}, io::prelude::*};

pub struct Credentials;

pub struct NisaApp {
    pub data: Option<NISA>,
    pub data_source: DataSource
}

impl NisaApp {
    pub fn data(&self) -> std::io::Result<NISA> {
        match &self.data {
            Some(nisa) => { Ok( nisa.clone() ) }, 
            None => {
                Ok( self.data_source.read().unwrap().clone() )
            }
        }
    }
}

#[derive(Clone)]
pub struct DataSource {
    pub context: Option<String>,
    pub data_source_type: DataSourceType,
    pub endpoint: String,
}

impl DataSource {
    pub fn file(context: Option<String>, endpoint: String) -> Self {
        DataSource {
            context,
            data_source_type: DataSourceType::File,
            endpoint
        }
    }
}

#[derive(Clone)]
pub enum DataSourceType { File, Persy }

pub trait CRUD {  // <T>
    fn create(&self);
    fn read(&self) -> std::io::Result<NISA>;
    fn update(&self, data: NISA) -> std::io::Result<()>; //<T>
    fn delete(&self);
}

impl CRUD for DataSource {  //<T>
    fn create(&self){}
    fn read(&self) -> std::io::Result<NISA> { // <NISA>
        let mut file = File::open(&self.endpoint)?;
        let mut contents = String::new();
        file.read_to_string(&mut contents)?;
        let nisa: NISA = serde_json::from_str(&contents).unwrap();
        Ok(nisa)
    }
    fn update(&self, data: NISA) -> std::io::Result<()> {
        let serialized = serde_json::to_string(&data).unwrap();
        std::fs::write(self.endpoint.clone(), &serialized)?;
        Ok(())
    }
    fn delete(&self){}
}
