var changes = false;
$(document).ready(function() {
    
    $ ('[id^=img-file-input][type=file]').on('click', function (evt) {
        var file = evt.target.files[0];
      
        if (file.type.match('image.*')) {
            var id = this.id.split("_")[1];
            console.log(id);
            var reader = new FileReader();
            reader.onload = function(evt) {
                $("#thumb_" + id).attr("src", evt.target.result);
            }
            reader.readAsDataURL(file);
        }
        

        // var short =  $(this).parents()[3].id.split("_")[2];
        // var fd = new FormData();
        // fd.append('file', this.files[0]);
        // $.ajax({
        //     method: "POST",
        //     url: "/upload?short=" + short,
        //     global: false,
        //     data: fd,
        //     dataType: "json",
        //     error: function(e) {
        //         $('#msg_title').text("Error!");
        //         $('#msg').text(e);
        //         $('.alert').removeClass("invisible");
        //         $('.alert').addClass("visible");
        //     },
        //     success: function(msg) {
        //         $('#msg_title').text("Success!");
        //         $('#msg').text(msg);
        //         $('.alert').removeClass("invisible");
        //         $('.alert').addClass("visible");
               
        //     },
        //     contentType : false
        // });
    })
});

function add() {
    var add_count = $(".club").length; 
    console.log(add_count);

    $.ajax({ type: "GET",   
         url: "/assets/clubs_form_group.hbs",   
        error: function() {
            $('#msg_title').text("Error!");
            $('#msg').text("Couldn't add load form-group");
            $('.alert').removeClass("invisible");
            $('.alert').addClass("visible");
        },
         success : function(text) {
            $('#clubs').prepend(text);
            $("#full_name").first().attr("name", "clubs[" + add_count + "].full_name");
            $("#trivial_name").first().attr("name", "clubs[" + add_count + "].trivial_name");
            $("#short").first().attr("name", "clubs[" + add_count + "].short");
            $("#color_primary").first().attr("name", "clubs[" + add_count + "].colors.primary");
            $("#color_secondary").first().attr("name", "clubs[" + add_count + "].colors.secondary");
            $("#color_tertiary").first().attr("name", "clubs[" + add_count + "].colors.tertiary");
            $("#img-file-input_new").first().attr("id", "img-file-input_" + add_count);
         }
    });
}
function save() {
    var form = document.querySelector('#data');
    var serialized = serializeJson(form); 
    console.log(serialized.clubs);
    $.ajax({
        method: "POST",
        url: "/all/clubs",
        global: false,
        data: JSON.stringify(serialized.clubs),
        dataType: "json",
        error: function(e) {
            $('#msg_title').text("Error!");
            $('#msg').text(e);
            $('.alert').removeClass("invisible");
            $('.alert').addClass("visible");
        },
        success: function(msg) {
            $('#msg_title').text("Success!");
            $('#msg').text(msg);
            $('.alert').removeClass("invisible");
            $('.alert').addClass("visible");
           
        },
        contentType : "application/json"
    });
}  