var changes = false;
$(document).ready(function() {
    
});

function add() {
    var add_count = $(".match").length; 
    console.log(add_count);

    $.ajax({ type: "GET",   
         url: "/assets/clubs_form_group.hbs",   
        error: function() {
            $('#msg_title').text("Error!");
            $('#msg').text("Couldn't add load form-group");
            $('.alert').removeClass("invisible");
            $('.alert').addClass("visible");
        },
        success : function(text) {
            $('#clubs').prepend(text);
            $("#full_name").first().attr("name", "clubs[" + add_count + "].full_name");
            $("#trivial_name").first().attr("name", "clubs[" + add_count + "].trivial_name");
            $("#short").first().attr("name", "clubs[" + add_count + "].short");
            $("#color_primary").first().attr("name", "clubs[" + add_count + "].colors.primary");
            $("#color_secondary").first().attr("name", "clubs[" + add_count + "].colors.secondary");
            $("#color_tertiary").first().attr("name", "clubs[" + add_count + "].colors.tertiary");
            $("#img-file-input_new").first().attr("id", "img-file-input_" + add_count);
         }
    });
}
function save() {
    var form = document.querySelector('#data');
    var serialized = serializeJson(form); 
    console.log(serialized.clubs);
    $.ajax({
        method: "POST",
        url: "/all/matches",
        global: false,
        data: JSON.stringify(serialized.mazches),
        dataType: "json",
        error: function(e) {
            $('#msg_title').text("Error!");
            $('#msg').text(e);
            $('.alert').removeClass("invisible");
            $('.alert').addClass("visible");
        },
        success: function(msg) {
            $('#msg_title').text("Success!");
            $('#msg').text(msg);
            $('.alert').removeClass("invisible");
            $('.alert').addClass("visible");
           
        },
        contentType : "application/json"
    });
}  